package com.example.drinksapp.repository

import android.content.Context
import android.hardware.camera2.CaptureFailure
import com.example.drinksapp.entities.DetailsList
import com.example.drinksapp.entities.Drink
import com.example.drinksapp.entities.DrinkDetail
import com.example.drinksapp.entities.DrinksList
import com.example.drinksapp.network.RetrofitInitializer
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class DrinksRepository(private val context: Context) {

    companion object{
        val bartender = RetrofitInitializer().createBartender()
    }

    fun getAlcoholicList(onSuccess: ((drinks: List<Drink>, isFromCache: Boolean) -> Unit),
                         onFailure: ((t: Throwable) -> Unit)){


        val paperKey = "AlcoholicsList"

        var drinks: List<Drink>?

        val callback : Callback<DrinksList> =
            (object : Callback<DrinksList> {
            override fun onFailure(call: Call<DrinksList>, t: Throwable) {
                drinks = Paper.book().read(paperKey)

                if(drinks != null){
                    onSuccess(drinks!!, true)
                } else {
                    onFailure(t)
                }
            }

            override fun onResponse(call: Call<DrinksList>, response: Response<DrinksList>) {
                drinks = response.body()?.drinks

                if (drinks != null) {
                    Paper.book().write(paperKey,drinks)
                    onSuccess(drinks!!, false)
                } else {
                    drinks = Paper.book().read(paperKey)
                    if (drinks != null) {
                        onSuccess(drinks!!,true)
                    } else{
                        onFailure(Exception("Não existem dados para serem carregados"))
                    }

                }
            }
        })

        bartender.getAlcoholicDrinks().enqueue(callback)
    }

    fun getDrinkByID(id: String,
                     onSuccess: ((detail: DrinkDetail, isFromCache: Boolean) -> Unit),
                     onFailure: ((t: Throwable) -> Unit)){

        var paperKey = id //para o cache não ficar "burro", fazendo cache de um drink só

        var detail: DrinkDetail?

        val callback: Callback<DetailsList> =
            (object : Callback<DetailsList> {
                override fun onFailure(call: Call<DetailsList>, t: Throwable) {
                    call.request()
                    detail = Paper.book().read(paperKey)
                    if (detail != null){
                        onSuccess(detail!!,true)
                    } else{
                        onFailure(t)
                    }
                }

                override fun onResponse(call: Call<DetailsList>, response: Response<DetailsList>) {
                    call.request()
                    detail = Paper.book().read(paperKey)
                    if (detail != null){
                        onSuccess(detail!!,true)
                    } else{
                        detail = response.body()?.drinks?.get(0)
                        Paper.book().write(paperKey,detail)
                        onSuccess(detail!!,false)
                    }

                }

            })

        bartender.getDrinkByID(id).enqueue(callback)

    }
}