package com.example.drinksapp.scenarios.scene_drink_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.drinksapp.R
import com.example.drinksapp.common.BackButtonListener
import com.example.drinksapp.common.FlowProvider
import com.example.drinksapp.common.Screens
import com.example.drinksapp.entities.DrinkDetail
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_drink_list.*
import kotlinx.android.synthetic.main.fragment_drink_list.pbLoading
import ru.terrakok.cicerone.commands.Back

class DrinkDetailFragment: Fragment(),DrinkDetailContract.View{

    companion object{


        fun newInstance(id: String): DrinkDetailFragment{
            val fragment = DrinkDetailFragment()
            fragment.arguments = Bundle().apply {
                putString("id", id)
            }
            return fragment
        }

    }

    val router by lazy {
        (this.activity as FlowProvider).provideRouter()
    }
    override fun hideLoading() {
        pbLoadingDetails?.visibility = ProgressBar.INVISIBLE
        fmMaster?.visibility = FrameLayout.VISIBLE
    }

    override fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        detailsPlaceholder.visibility = FrameLayout.INVISIBLE
        fmMaster?.visibility = FrameLayout.INVISIBLE
        pbLoadingDetails?.visibility = ProgressBar.VISIBLE
    }

    override fun showDetails(detail: DrinkDetail) {

        Glide.with(this)
            .load(detail.strDrinkThumb)
            .circleCrop()
            .into(this.imgDrinkDetail)

        this.txDrinkName.text = detail.strDrink
        this.txIsAlcoholic.text = detail.strAlcoholic
        this.txGlass.text = detail.strGlass
        this.txInstructions.text = detail.strInstructions

        lvIngredientsList?.adapter = IngredientsListAdapter(detail)
        lvIngredientsList.layoutManager = LinearLayoutManager(activity)

        imgDrinkDetail.parent.requestChildFocus(imgDrinkDetail,imgDrinkDetail)

        detailsPlaceholder?.visibility = FrameLayout.VISIBLE


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val drinkId = getDrinkId()
        activity?.let {activity ->
            val presenter: DrinkDetailContract.Presenter =
                DrinkDetailPresenter(drinkId!!, this, activity)
                presenter.loadDetails()
    }
    }

    private fun getDrinkId() = arguments?.getString("id")

    override fun showError() {
        val drinkId = getDrinkId()
        detailsPlaceholder.visibility = FrameLayout.INVISIBLE
        fmErrorDetails.visibility = FrameLayout.VISIBLE

        errorDetailsButton?.setOnClickListener{
            fmErrorDetails.visibility = FrameLayout.INVISIBLE
            activity?.let {activity ->
                val presenter: DrinkDetailContract.Presenter =
                    DrinkDetailPresenter(drinkId!!, this, activity)
                    presenter.loadDetails()
            }
        }

    }


    }










