package com.example.drinksapp.scenarios.scene_drinks_list

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideContext
import com.example.drinksapp.R
import com.example.drinksapp.entities.Drink
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.drink_item.view.*

class DrinksListAdapter (private val context: Context,
                         drinks: List<Drink>,
                         val itemClickListener: ((drink: Drink) -> Unit)) : GroupAdapter<GroupieViewHolder>(){

    init {
        drinks.forEach { drink ->
            add(DrinkItem(drink))
        }
    }


    inner class DrinkItem(val drink : Drink) : Item<GroupieViewHolder>(){

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.tvTitle.text = drink.strDrink

            Glide.with(context)
                .load(drink.strDrinkThumb)
                .placeholder(R.drawable.cocktail)
                .circleCrop()
                .into(viewHolder.itemView.imgDrink)

            viewHolder.itemView.setOnClickListener{
                itemClickListener(drink)
            }
        }

        override fun getLayout(): Int = R.layout.drink_item
    }
}