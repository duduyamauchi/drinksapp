package com.example.drinksapp.scenarios.scene_drinks_list

import com.example.drinksapp.entities.Drink
import com.example.drinksapp.entities.DrinksList

interface DrinksListContract {

    interface Presenter{
        fun onLoadList()

    }

    interface View{
        fun showToast(msg: String)
        fun showError()
        fun showList(drinks: List<Drink>)
        fun showLoading()
        fun hideLoading()
    }

}