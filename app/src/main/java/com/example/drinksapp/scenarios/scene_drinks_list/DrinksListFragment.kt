package com.example.drinksapp.scenarios.scene_drinks_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drinksapp.R
import com.example.drinksapp.common.BackButtonListener
import com.example.drinksapp.common.FlowProvider
import com.example.drinksapp.common.MainActivity
import com.example.drinksapp.common.Screens
import com.example.drinksapp.entities.Drink
import com.example.drinksapp.scenarios.scene_drink_detail.DrinkDetailFragment
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_drink_list.*
import kotlinx.android.synthetic.main.fragment_drink_list.pbLoading
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Screen
import kotlin.random.Random

class DrinksListFragment : Fragment(), DrinksListContract.View {



    companion object {

        fun newInstance(): DrinksListFragment {

            return DrinksListFragment()
        }
    }

    val router by lazy {
        (this.activity as FlowProvider).provideRouter() //tem que fazer assim pq o "parent" é uma activity e não um fragment
    }


    override fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showError() {
        listPlaceholder.visibility = FrameLayout.INVISIBLE
        fmErrorList.visibility = FrameLayout.VISIBLE

        errorListButton?.setOnClickListener {
            fmErrorList.visibility = FrameLayout.INVISIBLE
            activity?.let {
                val presenter: DrinksListContract.Presenter = DrinksListPresenter(this, it)
                presenter.onLoadList()
            }
        }

    }

    override fun showList(drinks: List<Drink>) {

        activity?.let { activity ->
            val adapter = DrinksListAdapter(activity, drinks) {
                router.navigateTo(Screens.DrinkDetailScreen(it.idDrink))
            }
            rvDrinks?.adapter = adapter
            rvDrinks?.layoutManager = LinearLayoutManager(activity)

            randomDrinkButton?.setOnClickListener() {
                val randomDrink = drinks[Random.nextInt(0, drinks.size)]
                router.navigateTo(Screens.DrinkDetailScreen(randomDrink.idDrink))
            }
        }
    }

    override fun showLoading() {
        listPlaceholder?.visibility = FrameLayout.INVISIBLE
        pbLoading?.visibility = ProgressBar.VISIBLE
    }

    override fun hideLoading() {
        pbLoading?.visibility = ProgressBar.INVISIBLE
        listPlaceholder?.visibility = FrameLayout.VISIBLE
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_drink_list, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            val presenter: DrinksListContract.Presenter = DrinksListPresenter(this, it)
            presenter.onLoadList()
        }

    }


}