package com.example.drinksapp.scenarios.scene_drink_detail

import android.graphics.Typeface
import android.util.TypedValue
import androidx.annotation.Dimension
import com.example.drinksapp.R
import com.example.drinksapp.entities.Drink
import com.example.drinksapp.entities.DrinkDetail
import com.example.drinksapp.scenarios.scene_drinks_list.DrinksListAdapter
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.ingredients_list_item.view.*

class IngredientsListAdapter(val detail: DrinkDetail) :GroupAdapter<GroupieViewHolder>() {



    init {

        add(IngredientsListItem(-1))
        for (i in 0..14) {
            if (detail.getIngredients()[i] != null) { //você está errado, android studio, pode ser null sim
                add(IngredientsListItem(i))
            }
        }
    }

    inner class IngredientsListItem(val index: Int) : Item<GroupieViewHolder>() {
        override fun getLayout(): Int = R.layout.ingredients_list_item

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            if(index >= 0){
                viewHolder.itemView.txIngrediente.text = detail.getIngredients()[index]
                viewHolder.itemView.txQuantidade.text = detail.getMeasures()[index]
            } else{
                viewHolder.itemView.txIngrediente.setTypeface(viewHolder.itemView.txIngrediente.typeface,Typeface.NORMAL)
                //viewHolder.itemView.txQuantidade.setTypeface(viewHolder.itemView.txQuantidade.typeface,Typeface.NORMAL)
                viewHolder.itemView.txIngrediente.text = "Ingredient"
                viewHolder.itemView.txQuantidade.text = "Quantity"
            }

        }
    }
}