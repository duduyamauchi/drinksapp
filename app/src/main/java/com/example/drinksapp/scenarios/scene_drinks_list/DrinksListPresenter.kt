package com.example.drinksapp.scenarios.scene_drinks_list

import android.content.Context
import com.example.drinksapp.entities.Drink
import com.example.drinksapp.entities.DrinksList
import com.example.drinksapp.network.RetrofitInitializer
import com.example.drinksapp.repository.DrinksRepository
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class DrinksListPresenter (val view: DrinksListContract.View, val context: Context) : DrinksListContract.Presenter {
    override fun onLoadList() {

        view.showLoading()

        val repository = DrinksRepository(context)

        val onSuccess: ((drinks: List<Drink>, isFromCache: Boolean) -> Unit) = {drinks, isFromCache ->
            view.hideLoading()
            if (isFromCache) view.showToast("Drinks carregados do cache")
            view.showList(drinks)
        }

        val onFailure: ((t: Throwable) -> Unit) = {
            view.hideLoading()
            view.showError()
        }

        repository.getAlcoholicList(onSuccess, onFailure)

    }
}