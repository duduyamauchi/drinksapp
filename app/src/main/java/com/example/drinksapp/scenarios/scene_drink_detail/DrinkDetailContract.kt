package com.example.drinksapp.scenarios.scene_drink_detail

import android.telecom.Call
import com.example.drinksapp.entities.Drink
import com.example.drinksapp.entities.DrinkDetail

interface DrinkDetailContract {

    interface View{
        fun showDetails(detail: DrinkDetail)
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun showToast(msg: String)
    }

    interface Presenter{
        fun loadDetails()
    }
}