package com.example.drinksapp.scenarios.scene_drink_detail

import android.content.Context
import com.example.drinksapp.entities.DrinkDetail
import com.example.drinksapp.repository.DrinksRepository

class DrinkDetailPresenter(val drinkId: String, val view: DrinkDetailContract.View,val context: Context) : DrinkDetailContract.Presenter {
    override fun loadDetails() {
        view.showLoading()

        val repository = DrinksRepository(context)

        val onSuccess: ((detail: DrinkDetail, isFromCache: Boolean) -> Unit) = {detail, isFromCache ->
            view.hideLoading()
            if (isFromCache) view.showToast("Detalhe carregado do cache")
            view.showDetails(detail)
        }

        val onFailure: ((t: Throwable) -> Unit) = {
            view.hideLoading()
            view.showError()
        }

        repository.getDrinkByID(drinkId,onSuccess,onFailure)

    }
}