package com.example.drinksapp.common


import androidx.fragment.app.Fragment
import com.example.drinksapp.scenarios.scene_drink_detail.DrinkDetailFragment
import com.example.drinksapp.scenarios.scene_drinks_list.DrinksListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {

    class DrinksListScreen: SupportAppScreen(){
        override fun getFragment(): Fragment {
            return DrinksListFragment()
        }
    }

    class DrinkDetailScreen(val id: String): SupportAppScreen(){
        override fun getFragment(): Fragment {
            return DrinkDetailFragment.newInstance(id)
        }
    }

}