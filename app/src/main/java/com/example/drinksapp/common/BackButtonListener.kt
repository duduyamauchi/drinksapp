package com.example.drinksapp.common

interface BackButtonListener {
    fun onBackPressed()
}