package com.example.drinksapp.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import androidx.fragment.app.FragmentActivity
import com.example.drinksapp.R
import com.example.drinksapp.scenarios.scene_drinks_list.DrinksListFragment
import io.paperdb.Paper
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class MainActivity : AppCompatActivity(), FlowProvider{

    val cicerone = Cicerone.create()
    val navigator by lazy {
        SupportAppNavigator(this as FragmentActivity,supportFragmentManager,R.id.fmMaster)
    }

    override fun provideRouter(): Router {
        return cicerone.router
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        Paper.init(this)

        cicerone.router.replaceScreen(Screens.DrinksListScreen())

//        val drinksListFragment =
//            DrinksListFragment.newInstance()
//        supportFragmentManager.beginTransaction()
//            .replace(R.id.fmMaster, drinksListFragment)
//            .commitNow()
    }

    override fun onResume() {
        super.onResume()
        cicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        cicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }


}


