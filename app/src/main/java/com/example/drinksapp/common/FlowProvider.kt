package com.example.drinksapp.common

import ru.terrakok.cicerone.Router

interface FlowProvider {
    fun provideRouter(): Router
}