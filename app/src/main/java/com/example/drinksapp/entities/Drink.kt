package com.example.drinksapp.entities


data class Drink(
    val strDrink: String,
    val strDrinkThumb: String,
    val idDrink: String
)

