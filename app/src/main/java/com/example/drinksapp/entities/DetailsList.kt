package com.example.drinksapp.entities

data class DetailsList (val drinks: List<DrinkDetail>)
