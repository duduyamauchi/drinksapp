package com.example.drinksapp.entities

data class DrinksList (val drinks: List<Drink>)