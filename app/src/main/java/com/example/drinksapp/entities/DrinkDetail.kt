package com.example.drinksapp.entities

data class DrinkDetail (val idDrink: String,
                        val strDrink: String,
                        val strAlcoholic: String,
                        val strGlass: String,
                        val strInstructions: String,
                        var strIngredient1: String,
                        val strIngredient2: String,
                        val strIngredient3: String,
                        val strIngredient4: String,
                        val strIngredient5: String,
                        val strIngredient6: String,
                        val strIngredient7: String,
                        val strIngredient8: String,
                        val strIngredient9: String,
                        val strIngredient10: String,
                        val strIngredient11: String,
                        val strIngredient12: String,
                        val strIngredient13: String,
                        val strIngredient14: String,
                        val strIngredient15: String,
                        var strMeasure1: String,
                        val strMeasure2: String,
                        val strMeasure3: String,
                        val strMeasure4: String,
                        val strMeasure5: String,
                        val strMeasure6: String,
                        val strMeasure7: String,
                        val strMeasure8: String,
                        val strMeasure9: String,
                        val strMeasure10: String,
                        val strMeasure11: String,
                        val strMeasure12: String,
                        val strMeasure13: String,
                        val strMeasure14: String,
                        val strMeasure15: String,
                        val strDrinkThumb: String){

    fun getIngredients():ArrayList<String>{

        val list = ArrayList<String>()

        list.add(strIngredient1)
        list.add(strIngredient2)
        list.add(strIngredient3)
        list.add(strIngredient4)
        list.add(strIngredient5)
        list.add(strIngredient6)
        list.add(strIngredient7)
        list.add(strIngredient8)
        list.add(strIngredient9)
        list.add(strIngredient10)
        list.add(strIngredient11)
        list.add(strIngredient12)
        list.add(strIngredient13)
        list.add(strIngredient14)
        list.add(strIngredient15)

        return list

    }

    fun getMeasures():ArrayList<String>{

        val list = ArrayList<String>()

        list.add(strMeasure1)
        list.add(strMeasure2)
        list.add(strMeasure3)
        list.add(strMeasure4)
        list.add(strMeasure5)
        list.add(strMeasure6)
        list.add(strMeasure7)
        list.add(strMeasure8)
        list.add(strMeasure9)
        list.add(strMeasure10)
        list.add(strMeasure11)
        list.add(strMeasure12)
        list.add(strMeasure13)
        list.add(strMeasure14)
        list.add(strMeasure15)

        return list

    }


}