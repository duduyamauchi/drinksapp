package com.example.drinksapp.network

import android.hardware.usb.UsbEndpoint
import com.example.drinksapp.entities.DetailsList
import com.example.drinksapp.entities.Drink
import com.example.drinksapp.entities.DrinkDetail
import com.example.drinksapp.entities.DrinksList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Bartender {

    companion object{
        public const val ENDPOINT = "https://www.thecocktaildb.com/api/json/v1/1/"

    }

    @GET("filter.php?a=Alcoholic")
    fun getAlcoholicDrinks() : Call<DrinksList>

    @GET("lookup.php")
    fun getDrinkByID(@Query("i") id : String) : Call<DetailsList>


}