package com.example.drinksapp.network

import com.example.drinksapp.network.Bartender.Companion.ENDPOINT
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.internal.GsonBuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

    val retrofit = Retrofit.Builder()
        .baseUrl("${ENDPOINT}")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun createBartender() = retrofit.create(Bartender::class.java)
}